# Aplikacja SOS

## Utworzenie bazy danych
W głównym katalogu (hsqldb) wydaj komendę w linii poleceń:

```bash
java -classpath lib/hsqldb.jar org.hsqldb.server.Server
```

## Zarządzanie bazą danych
Wszystkie polecenia należy wykonywać z poziomu katalogu: `Zadanie2/Sos/hsqldb`

### Uruchomienie serwera HSQLDB
```bash
java -classpath lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/hemrajdb --dbname.0 testdb
```

### Uruchomienie klienta bazy danych
```bash
java -classpath lib/hsqldb.jar org.hsqldb.util.DatabaseManagerSwing
```