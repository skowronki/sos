DROP TABLE IF EXISTS wykladowcy;
CREATE TABLE wykladowcy
(
    id_wykladowca   IDENTITY PRIMARY KEY,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL,
    telefon         VARCHAR(12) NOT NULL,
    email           VARCHAR(150) NOT NULL
);

DROP TABLE IF EXISTS przedmioty;
CREATE TABLE przedmioty
(
    id_przedmiot        IDENTITY PRIMARY KEY,
    id_wykladowca       INT NOT NULL,
    nazwa               VARCHAR(255) NOT NULL,

    FOREIGN KEY (id_wykladowca) REFERENCES wykladowcy (id_wykladowca)
);

DROP TABLE IF EXISTS grupy_studentow;
CREATE TABLE grupy_studentow
(
    id_grupa            IDENTITY PRIMARY KEY,
    rok_akademicki      INT NOT NULL,
    nazwa_grupy         VARCHAR(255) NOT NULL,
    semestr             INT NOT NULL
);

DROP TABLE IF EXISTS plany_zajec;
CREATE TABLE plany_zajec
(
    id              IDENTITY PRIMARY KEY,
    id_plan         INT NOT NULL,
    id_grupa        INT NOT NULL,
    id_wykladowca   INT NOT NULL,
    id_przedmiot    INT NOT NULL,
    data            VARCHAR(255) NOT NULL,
    id_sala         INT NOT NULL,
    godzina         INT NOT NULL,
    minuta          INT NOT NULL
);

DROP TABLE IF EXISTS sale;
CREATE TABLE sale
(
    id_sala          IDENTITY PRIMARY KEY,
    numer_sali       INT NOT NULL,
    pietro           INT NOT NULL,
    typ              VARCHAR(150) ARRAY NOT NULL, /* typy: sala wykladowa, sekretariat, dziekanat, biblioteka, toaleta */
    opis             VARCHAR(255) NOT NULL
);

DROP TABLE IF EXISTS oceny;
CREATE TABLE oceny
(
    id_ocena        IDENTITY PRIMARY KEY,
    id_student      INT NOT NULL,
    id_wykladowca   INT NOT NULL,
    id_przedmiot   INT NOT NULL,
    semestr         INT NOT NULL,
    ocena           INT NOT NULL
);

DROP TABLE IF EXISTS system_biblioteczny;
CREATE TABLE system_biblioteczny
(
    id_sb               IDENTITY PRIMARY KEY,
    id_ksiazka          INT NOT NULL,
    id_konto            INT NOT NULL,
    data_wypozyczenia   VARCHAR(255) NOT NULL,
    data_zwrotu         VARCHAR(255) NOT NULL,
    opoznienie          DATE NOT NULL,
    kolejka             INT NOT NULL
);

DROP TABLE IF EXISTS studenci;
CREATE TABLE studenci
(
    id_student  IDENTITY PRIMARY KEY,
    id_grupa    INT NOT NULL,
    id_sb       INT NOT NULL,
    nr_indeksu  INT NOT NULL,
    id_konto    INT NOT NULL,
    imie        VARCHAR(255) NOT NULL,
    nazwisko    VARCHAR(255) NOT NULL,
    adres       VARCHAR(255) NOT NULL,
    telefon     VARCHAR(12) NOT NULL,
    pesel       DECIMAL(11,0) NOT NULL,
    FOREIGN KEY (id_grupa) REFERENCES grupy_studentow (id_grupa)
);

DROP TABLE IF EXISTS konto;
CREATE TABLE konto
(
    id_konto    IDENTITY PRIMARY KEY,
    id_student  INT NOT NULL,
    login       VARCHAR(255) NOT NULL,
    haslo       VARCHAR(255) NOT NULL,
    email       VARCHAR(255) NOT NULL,
    status      VARCHAR(255) NOT NULL,
    data_od     VARCHAR(255) NOT NULL,
    data_do     VARCHAR(255) NOT NULL,
    licznik_negatywnych_prob_logowania  INT NOT NULL,
    FOREIGN KEY (id_student) REFERENCES studenci (id_student)
);

DROP TABLE IF EXISTS system_ksiegowy;
CREATE TABLE system_ksiegowy
(
    id_czesne       IDENTITY PRIMARY KEY,
    id_konto        INT NOT NULL,
    czy_zaplata     BOOLEAN NOT NULL,
    ile_zaplata     DECIMAL(10, 2) NOT NULL,
    kiedy_zaplata   DATE NOT NULL
);

DROP TABLE IF EXISTS ksiazki;
CREATE TABLE ksiazki
(
    id_ksiazka          IDENTITY PRIMARY KEY,
    tytul               VARCHAR(255) NOT NULL,
    autor               VARCHAR (255) NOT NULL,
    wydawca             VARCHAR(255) NOT NULL,
    isbn                VARCHAR(255) NOT NULL,
    rok_wydania         DATE NOT NULL
);