/*
 * Grupa Skowronki
 * Zbieżność osób i nazwisk jest przypadkowa
 */

INSERT INTO wykladowcy(id_wykladowca, imie, nazwisko, telefon, email)
VALUES (1, 'Cezary', 'Hołub', '48111222333', 'cholub@horyzont.eu');

INSERT INTO przedmioty(id_przedmiot, id_wykladowca, nazwa)
VALUES (1, 1, 'Projektowanie i programowanie aplikacji biznesowych');

INSERT INTO grupy_studentow(id_grupa, rok_akademicki, nazwa_grupy, semestr)
VALUES (1, 2022, 'A', 4), (2, 2022, 'B', 4);

INSERT INTO sale(id_sala, numer_sali, pietro, typ, opis)
VALUES (1, 10, 1, ARRAY['sala wykladowa'], 'Sala komputerowa');

INSERT INTO plany_zajec(id, id_plan, id_grupa, id_wykladowca, id_przedmiot, data, id_sala, godzina, minuta)
VALUES
(1, 1, 1, 1, 1, '2022-06-11', 1, 9, 15),
(2, 1, 2, 1, 1, '2022-06-04', 1, 9, 15);

INSERT INTO studenci(id_student, id_grupa, id_sb, nr_indeksu, id_konto, imie, nazwisko, adres, telefon, pesel)
VALUES
(1, 2, 1, 6938, 1, 'Tomasz', 'Piwowarski', 'Router, Światłowód widmo 3', 'Poco', 12345678901),
(2, 2, 2, 6929, 1, 'Emil', 'Dworniczak', 'Rumunia, Transylwania', 'Viko', 11222233344),
(3, 2, 3, 0001, 1, 'Iwona', 'Zając', 'Zwierzogród, Szarakówka', 'Samsung', 11222233345),
(4, 2, 4, 0002, 1, 'Marcin', 'Gryboś', 'Zasiedmiogurogrodzie, Duloc', 'Nokia 3310', 11222233346);

INSERT INTO oceny(id_ocena, id_student, id_wykladowca, id_przedmiot, semestr, ocena)
VALUES
(1, 1, 1, 1, 4, 5),
(2, 2, 1, 1, 4, 5),
(3, 3, 1, 1, 4, 5),
(4, 4, 1, 1, 4, 5);

INSERT INTO konto(id_konto, id_student, login, haslo, email, status, data_od, data_do, licznik_negatywnych_prob_logowania)
VALUES
(1, 1, 's6938', 'maxwell', 's6938@horyzont.eu', 'ACTIVE', '2020-06-04', '2023-06-04', 0),
(2, 2, 's6929', 'mentzen', 's6929@horyzont.eu', 'ACTIVE', '2020-06-04', '2023-06-04', 0),
(3, 3, '0001', 'nastya', 's0001@horyzont.eu', 'ACTIVE', '2020-06-04', '2023-06-04', 0),
(4, 4, '0002', 'omicron', 's0002@horyzont.eu', 'ACTIVE', '2020-06-04', '2023-06-04', 0);