@Entity
@Table(name="grupy_studentow")
public class GrupyStudentow {
    @Id
    private Long id_grupa;

    private Integer rok_akademicki;

    private String nazwa_grupy;

    private Integer semestr;

    public GrupyStudentow(Long id_grupa, Integer rok_akademicki, String nazwa_grupy, Integer semestr) {
        super();
        this.id_grupa = id_grupa;
        this.rok_akademicki = rok_akademicki;
        this.nazwa_grupy = nazwa_grupy;
        this.semestr = semestr;
    }

    public Long getId_grupa() {
        return id_grupa;
    }

    public void setId_grupa(Long id_grupa) {
        this.id_grupa = id_grupa;
    }

    public void setRok_akademicki(Integer rok_akademicki) {
        this.rok_akademicki = rok_akademicki;
    }

    public Integer getRok_akademicki() {
        return rok_akademicki;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public String getNazwa_grupy() {
        return nazwa_grupy;
    }

    public void setNazwa_grupy(String nazwa_grupy) {
        this.nazwa_grupy = nazwa_grupy;
    }
}