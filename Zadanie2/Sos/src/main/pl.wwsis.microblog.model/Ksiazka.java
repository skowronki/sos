@Entity
@Table(name="ksiazki")
public class Ksiazki {
    @Id
    private Long id_ksiazka;
    private String tytul;
    private String autor;
    private String wydawca;
    private String isbn;
    private String rok_wydania;

    public Ksiazki(Long id_ksiazka, String tytul, String autor, String wydawca, String isbn, String rok_wydania) {
        super();
        this.id_ksiazka = id_ksiazka;
        this.tytul = tytul;
        this.autor = autor;
        this.wydawca = wydawca;
        this.isbn = isbn;
        this.isbn = rok_wydania;
    }

    public Long getId_ksiazka() {
        return id_ksiazka;
    }

    public void setId_ksiazka(Long id_ksiazka) {
        this.id_ksiazka = id_ksiazka;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getWydawca() {
        return wydawca;
    }

    public void setWydawca(String wydawca) {
        this.wydawca = wydawca;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getRok_wydania() {
        return rok_wydania;
    }

    public void setRok_wydania(String rok_wydania) {
        this.rok_wydania = rok_wydania;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}