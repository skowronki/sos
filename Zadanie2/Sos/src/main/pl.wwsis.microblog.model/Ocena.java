@Entity
@Table (name="oceny")
public class Ocena {
    @Id
    private Long id_ocena;
    private Long id_student;
    private Long id_wykladowca;
    private Long id_przedmiot;
    private Int semestr;
    private Int ocena;

    public Long getId_ocena() {
        return id_ocena;
    }

    public void setId_ocena(Long id_ocena) {
        this.id_ocena = id_ocena;
    }

    public Long getId_student() {
        return id_student;
    }

    public void setId_student(Long id_student) {
        this.id_student = id_student;
    }

    public Long getId_wykladowca() {
        return id_wykladowca;
    }

    public void setId_wykladowca(Long id_wykladowca) {
        this.id_wykladowca = id_wykladowca;
    }

    public Long getId_przedmiot() {
        return id_przedmiot;
    }

    public void setId_przedmiot(Long id_przedmiot) {
        this.id_przedmiot = id_przedmiot;
    }

    public Int getSemestr() {
        return semestr;
    }

    public void setSemestr(Int semestr) {
        this.semestr = semestr;
    }

    public Int getOcena() {
        return ocena;
    }

    public void setOcena(Int ocena) {
        this.ocena = ocena;
    }

    public Ocena(Long id_ocena, Long id_student, Long id_wykladowca, Long id_przedmiot, Int semestr, Int ocena) {
        super();
        this.id_ocena = id_ocena;
        this.id_student = id_student;
        this.id_wykladowca = id_wykladowca;
        this.id_przedmiot = id_przedmiot;
        this.semestr = semestr;
        this.ocena = ocena;
    }
}