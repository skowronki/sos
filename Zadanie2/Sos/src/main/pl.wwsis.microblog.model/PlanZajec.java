@Entity
@Table (name="plany_zajec")
public class PlanyZajec {
    @Id
    private Long id;
    private Long id_plan;
    private Long id_grupa;
    private Long id_wykladowca;
    private Long id_przedmiot;
    private String data;
    private Long id_sala;
    private Int godzina;
    private Int minuta;

    public PlanyZajec(Long id, Long id_plan, Long id_grupa, Long id_wykladowca, Long id_przedmiot, String data
    Long id_sala, Int godzina, Int minuta) {
        super();
        this.id = id;
        this.id_plan = id_plan;
        this.id_grupa = id_grupa;
        this.id_wykladowca = id_wykladowca;
        this.id_przedmiot = id_przedmiot;
        this.data = data;
        this.id_sala = id_sala;
        this.godzina = godzina;
        this.minuta = minuta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_plan() {
        return id_plan;
    }

    public void setId_plan(Long id_plan) {
        this.id_plan = id_plan;
    }

    public Long getId_grupa() {
        return id_grupa;
    }

    public void setId_grupa(Long id_grupa) {
        this.id_grupa = id_grupa;
    }

    public Long getId_wykladowca() {
        return id_wykladowca;
    }

    public void setId_wykladowca(Long id_wykladowca) {
        this.id_wykladowca = id_wykladowca;
    }

    public Long getId_przedmiot() {
        return id_przedmiot;
    }

    public void setId_przedmiot(Long id_przedmiot) {
        this.id_przedmiot = id_przedmiot;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getId_sala() {
        return id_sala;
    }

    public void setId_sala(Long id_sala) {
        this.id_sala = id_sala;
    }

    public Int getGodzina() {
        return godzina;
    }

    public void setGodzina(Int godzina) {
        this.godzina = godzina;
    }

    public Int getMinuta() {
        return minuta;
    }

    public void setMinuta(Int minuta) {
        this.minuta = minuta;
    }
}