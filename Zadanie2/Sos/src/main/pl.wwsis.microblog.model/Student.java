@Entity
@Table(name="studenci")
public class Studenci {
    @Id
    private Long id_studenta;
    private Long id_grupa;
    private Long id_sb;
    private Integer nr_ideksu;
    private Long id_konto;
    private String imie;
    private String nazwisko;
    private String adres;
    private Integer telefon;
    private Integer pesel;

    public Studenci(Long id_studenta, Long id_grupa, Long id_sb, Integer nr_ideksu, Long id_konto, String imie, String nazwisko, String adres, Integer telefon, Integer pesel) {
        this.id_studenta = id_studenta;
        this.id_grupa = id_grupa;
        this.id_sb = id_sb;
        this.nr_ideksu = nr_ideksu;
        this.id_konto = id_konto;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
        this.telefon = telefon;
        this.pesel = pesel;
    }

    public Long getId_studenta() {
        return id_studenta;
    }

    public void setId_studenta(Long id_studenta) {
        this.id_studenta = id_studenta;
    }

    public Long getId_grupa() {
        return id_grupa;
    }

    public void setId_grupa(Long id_grupa) {
        this.id_grupa = id_grupa;
    }

    public Long getId_sb() {
        return id_sb;
    }

    public void setId_sb(Long id_sb) {
        this.id_sb = id_sb;
    }

    public Integer getNr_ideksu() {
        return nr_ideksu;
    }

    public void setNr_ideksu(Integer nr_ideksu) {
        this.nr_ideksu = nr_ideksu;
    }

    public Long getId_konto() {
        return id_konto;
    }

    public void setId_konto(Long id_konto) {
        this.id_konto = id_konto;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public Integer getTelefon() {
        return telefon;
    }

    public void setTelefon(Integer telefon) {
        this.telefon = telefon;
    }

    public Integer getPesel() {
        return pesel;
    }

    public void setPesel(Integer pesel) {
        this.pesel = pesel;
    }
}