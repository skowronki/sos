@Entity
@Table(name="system_ksiegowy")
public class systemKsiegowy {
    @Id
    private Long id_czesne;
    private Integer id_konto;
    private Boolean czy_zaplata;
    private Decimal ile_zaplata;
    private Date kiedy_zaplata;

    public Long getId_czesne() {
        return id_czesne;
    }

    public void setId_czesne(Long id_czesne) {
        this.id_czesne = id_czesne;
    }

    public Integer getId_konto() {
        return id_konto;
    }

    public void setId_konto(Integer id_konto) {
        this.id_konto = id_konto;
    }

    public Boolean getCzy_zaplata() {
        return czy_zaplata;
    }

    public void setCzy_zaplata(Boolean czy_zaplata) {
        this.czy_zaplata = czy_zaplata;
    }

    public Decimal getIle_zaplata() {
        return ile_zaplata;
    }

    public void setIle_zaplata(Decimal ile_zaplata) {
        this.ile_zaplata = ile_zaplata;
    }

    public Date getKiedy_zaplata() {
        return kiedy_zaplata;
    }

    public void setKiedy_zaplata(Date kiedy_zaplata) {
        this.kiedy_zaplata = kiedy_zaplata;
    }

    public systemKsiegowy(Long id_czesne, Integer id_konto, Boolean czy_zaplata, Decimal ile_zaplata, Date kiedy_zaplata) {
        super();
        this.id_czesne = id_czesne;
        this.id_konto = id_konto;
        this.czy_zaplata = czy_zaplata;
        this.ile_zaplata = ile_zaplata;
        this.kiedy_zaplata = kiedy_zaplata;
    }
}