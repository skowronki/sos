@Entity
@Table(name="system_biblioteczny")
public class systemBiblioteczny {
    @Id
    private Long id_sb;
    private Integer id_ksiazka;
    private Integer id_konto;
    private String data_wypozyczenia;
    private String data_zwrotu;

    public Long getId_sb() {
        return id_sb;
    }

    public void setId_sb(Long id_sb) {
        this.id_sb = id_sb;
    }

    public Integer getId_ksiazka() {
        return id_ksiazka;
    }

    public void setId_ksiazka(Integer id_ksiazka) {
        this.id_ksiazka = id_ksiazka;
    }

    public Integer getId_konto() {
        return id_konto;
    }

    public void setId_konto(Integer id_konto) {
        this.id_konto = id_konto;
    }

    public String getData_wypozyczenia() {
        return data_wypozyczenia;
    }

    public void setData_wypozyczenia(String data_wypozyczenia) {
        this.data_wypozyczenia = data_wypozyczenia;
    }

    public String getData_zwrotu() {
        return data_zwrotu;
    }

    public void setData_zwrotu(String data_zwrotu) {
        this.data_zwrotu = data_zwrotu;
    }

    public Date getOpoznienie() {
        return opoznienie;
    }

    public void setOpoznienie(Date opoznienie) {
        this.opoznienie = opoznienie;
    }

    public Integer getKolejka() {
        return kolejka;
    }

    public void setKolejka(Integer kolejka) {
        this.kolejka = kolejka;
    }

    private Date opoznienie;
    private Integer kolejka;

    public systemBiblioteczny(Long id_sb, Integer id_ksiazka, Integer id_konto, String data_wypozyczenia, String data_zwrotu, Date opoznienie, Integer kolejka) {
        super();
        this.id_sb = id_sb;
        this.id_ksiazka = id_ksiazka;
        this.id_konto = id_konto;
        this.data_wypozyczenia = data_wypozyczenia;
        this.data_zwrotu = data_zwrotu;
        this.opoznienie = opoznienie;
        this.kolejka = kolejka;
    }
}