@Entity
@Table(name="wykladowcy")
public class Wykladowca {
    @Id
    private Long id_wykladowcy;
    private String imie;
    private String nazwisko;
    private String telefon;
    private String email;

    public Wykladowca(Long id_wykladowcy, String imie, String nazwisko, String telefon, String email) {
        super();
        this.id_wykladowcy = id_wykladowcy;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.telefon = telefon;
        this.email = email;
    }

    public Long getId_wykladowcy() {
        return id_wykladowcy;
    }

    public void setId_wykladowcy(Long id_wykladowcy) {
        this.id_wykladowcy = id_wykladowcy;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        if (telefon.length > 12) {
            return "numer telefonu jest za długi";
        }
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        if (!pattern.matcher(email).matches()) {
            return "ten email nie jest poprawny";
        }
        this.email = email;
    }
}

