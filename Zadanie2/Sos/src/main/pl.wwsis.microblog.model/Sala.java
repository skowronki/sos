@Entity
@Table(name="sale")
public class Sale {
    @Id
    private Long id_sala;

    private Integer numer_sali;

    private Integer pietro;

    private String typ;

    public Long getId_sala() {
        return id_sala;
    }

    public void setId_sala(Long id_sala) {
        this.id_sala = id_sala;
    }

    public Integer getNumer_sali() {
        return numer_sali;
    }

    public void setNumer_sali(Integer numer_sali) {
        this.numer_sali = numer_sali;
    }

    public Integer getPietro() {
        return pietro;
    }

    public void setPietro(Integer pietro) {
        this.pietro = pietro;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    private String opis;

    public Sale(Long id_sala, Integer numer_sali, Integer pietro, String typ, String opis) {
        super();
        this.id_sala = id_sala;
        this.numer_sali = numer_sali;
        this.pietro = pietro;
        this.typ = typ;
        this.opis = opis;
    }

}