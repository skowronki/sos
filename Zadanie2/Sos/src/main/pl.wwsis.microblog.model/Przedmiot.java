@Entity
@Table (name="przedmioty")
public class Przedmiot {
    @Id
    private Long id_przedmiot;
    private Long id_wykladowca;
    private String nazwa;

    public Przedmiot(Long id_przedmiot, Long id_wykladowca, String nazwa) {
        super();
        this.id_przedmiot = id_przedmiot;
        this.id_wykladowca = id_wykladowca;
        this.nazwa = nazwa;
    }

    public Long getId_przedmiot() {
        return id_przedmiot;
    }

    public void setId_przedmiot(Long id_przedmiot) {
        this.id_przedmiot = id_przedmiot;
    }

    public Long getId_wykladowca() {
        return id_wykladowca;
    }

    public void setId_wykladowca(Long id_wykladowca) {
        this.id_wykladowca = id_wykladowca;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}