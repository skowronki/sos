@Entity
@Table(name="konto")
public class Konto {
    @Id
    private Long id_konto;
    private Long id_student;
    private String login;
    private String haslo;
    private String email;
    private String status;
    private String data_od;
    private String data_do;
    private Int licznik_negatywnych_prob_logowania;

    public Konto(Long id_konto, Long id_student, String login, String haslo, String email, String status,
                 String data_od, String data_do, Int licznik_negatywnych_prob_logowania) {
        super();
        this.id_konto = id_konto;
        this.id_student = id_student;
        this.login = login;
        this.haslo = haslo;
        this.email = email;
        this.status = status;
        this.data_od = data_od;
        this.data_do = data_do;
    }

    public Long getId_konto() {
        return id_konto;
    }

    public void setId_konto(Long id_konto) {
        this.id_konto = id_konto;
    }

    public Long getId_student() {
        return id_student;
    }

    public void setId_student(Long id_student) {
        this.id_student = id_student;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData_od() {
        return data_od;
    }

    public void setData_od(String data_od) {
        this.data_od = data_od;
    }

    public String getData_do() {
        return data_do;
    }

    public void setData_do(String data_do) {
        this.data_do = data_do;
    }

    public Int getLicznik_negatywnych_prob_logowania() {
        return licznik_negatywnych_prob_logowania;
    }

    public void setLicznik_negatywnych_prob_logowania(Int licznik_negatywnych_prob_logowania) {
        this.licznik_negatywnych_prob_logowania = licznik_negatywnych_prob_logowania;
    }

}